//
// Created by Pavel Kasila on 7.05.24.
//

#include "chart_callout.h"

#include <QChart>
#include <QColor>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QPointF>
#include <QRectF>
#include <QString>
#include <QStyleOptionGraphicsItem>
#include <Qt>
#include <QtMinMax>
#include <QtNumeric>
#include <QtTypes>

namespace outfit::core::statistics {
ChartCallout::ChartCallout(QChart* chart) : QGraphicsItem(chart), chart_(chart) {
}

QRectF ChartCallout::boundingRect() const {
    const QPointF anchor = mapFromParent(chart_->mapToPosition(anchor_));
    QRectF rect;
    rect.setLeft(qMin(rect_.left(), anchor.x()));
    rect.setRight(qMax(rect_.right(), anchor.x()));
    rect.setTop(qMin(rect_.top(), anchor.y()));
    rect.setBottom(qMax(rect_.bottom(), anchor.y()));
    return rect;
}

void ChartCallout::paint(
    QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    (void)option;
    (void)widget;
    QPainterPath path;
    path.addRoundedRect(rect_, 5, 5);

    const QPointF anchor = mapFromParent(chart_->mapToPosition(anchor_));
    if (!rect_.contains(anchor)) {
        QPointF point1;
        QPointF point2;

        // establish the position of the anchor point in relation to rect_
        const bool above = anchor.y() <= rect_.top();
        const bool above_center = anchor.y() > rect_.top() && anchor.y() <= rect_.center().y();
        const bool below_center = anchor.y() > rect_.center().y() && anchor.y() <= rect_.bottom();
        const bool below = anchor.y() > rect_.bottom();

        const bool on_left = anchor.x() <= rect_.left();
        const bool left_of_center = anchor.x() > rect_.left() && anchor.x() <= rect_.center().x();
        const bool right_of_center = anchor.x() > rect_.center().x() && anchor.x() <= rect_.right();
        const bool on_right = anchor.x() > rect_.right();

        // get the nearest rect_ corner.
        const qreal x =
            (static_cast<int>(on_right) + static_cast<int>(right_of_center)) * rect_.width();
        const qreal y = (static_cast<int>(below) + static_cast<int>(below_center)) * rect_.height();
        const bool corner_case =
            (above && on_left) || (above && on_right) || (below && on_left) || (below && on_right);
        const bool vertical = qAbs(anchor.x() - x) > qAbs(anchor.y() - y);

        const qreal x1 =
            x + static_cast<int>(left_of_center) * 10 - static_cast<int>(right_of_center) * 20 +
            static_cast<int>(corner_case) * static_cast<int>(!vertical) *
                (static_cast<int>(on_left) * 10 - static_cast<int>(on_right) * 20);
        const qreal y1 =
            y + static_cast<int>(above_center) * 10 - static_cast<int>(below_center) * 20 +
            static_cast<int>(corner_case) * static_cast<int>(vertical) *
                (static_cast<int>(above) * 10 - static_cast<int>(below) * 20);

        point1.setX(x1);
        point1.setY(y1);

        const qreal x2 =
            x + static_cast<int>(left_of_center) * 20 - static_cast<int>(right_of_center) * 10 +
            static_cast<int>(corner_case) * static_cast<int>(!vertical) *
                (static_cast<int>(on_left) * 20 - static_cast<int>(on_right) * 10);

        const qreal y2 =
            y + static_cast<int>(above_center) * 20 - static_cast<int>(below_center) * 10 +
            static_cast<int>(corner_case) * static_cast<int>(vertical) *
                (static_cast<int>(above) * 20 - static_cast<int>(below) * 10);

        point2.setX(x2);
        point2.setY(y2);

        path.moveTo(point1);
        path.lineTo(anchor);
        path.lineTo(point2);
        path = path.simplified();
    }
    painter->setBrush(QColor(255, 255, 255));
    painter->drawPath(path);
    painter->setPen(Qt::black);
    painter->drawText(text_rect_, text_);
}

void ChartCallout::mousePressEvent(QGraphicsSceneMouseEvent* event) {
    event->setAccepted(true);
}

void ChartCallout::mouseMoveEvent(QGraphicsSceneMouseEvent* event) {
    if ((event->buttons() & Qt::LeftButton) != 0U) {
        setPos(mapToParent(event->pos() - event->buttonDownPos(Qt::LeftButton)));
        event->setAccepted(true);
    } else {
        event->setAccepted(false);
    }
}

void ChartCallout::setText(const QString& text) {
    text_ = text;
    const QFontMetrics metrics(font_);
    text_rect_ = metrics.boundingRect(QRect(0, 0, 150, 150), Qt::AlignLeft, text_);
    text_rect_.translate(5, 5);
    prepareGeometryChange();
    rect_ = text_rect_.adjusted(-5, -5, 5, 5);
}

void ChartCallout::setAnchor(QPointF point) {
    anchor_ = point;
}

void ChartCallout::updateGeometry() {
    prepareGeometryChange();
    setPos(chart_->mapToPosition(anchor_) + QPoint(10, -50));
}
}  // namespace outfit::core::statistics